double func1(double x);
double func2(double x);
int get_args_N(char ** argv);
int get_args_n(char ** argv, int k);
void get_points(const char * filename, struct point * p, int n);
void get_lines( double * l, int N);

/* returns the lenght of a vector from 0 to the intersection of lines: y = kx , line(p1, p2) */
double get_polar_intersection(struct point p1, struct point p2, double k);
void get_func_values (struct point * p, int n, double * l, int N, double * f);
double get_curr_angle(struct point p, int current);
void get_values_2(struct point * p, int n, double * l, int N, double * f);
void centrelize(const int n, struct point * p);

void execution_time(const clock_t begin);