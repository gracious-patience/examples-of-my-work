#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

struct coefs
{
	double cos;
	double sin;
};

struct point
{
	double x;
	double y;
};

class net_func
{
public:
	int N;
	double a_0, a_last;
	double * f;
	struct coefs * cfs;


public:
	net_func();
	net_func(const int n, double (*func)(double));
	net_func(const int n, double * func_values);
	net_func(const net_func & f, const net_func & g);
	net_func(const net_func & h);

	~net_func();

	void print_coefs();
	void write_rotation(const int m);

	double get_sin_coef (const int k);
	double get_cos_coef (const int k);

	double summate (const int j);
	double summate_rotate(const int j, const int m);	
};
