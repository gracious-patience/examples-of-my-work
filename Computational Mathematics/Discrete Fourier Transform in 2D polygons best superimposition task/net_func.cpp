#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "net_func.h"



net_func::net_func(const int n, double (*func)(double))
{
		int i, j, k;
		
		double cos_0, cos_last;

		N = n;

		f = (double *) malloc (N * sizeof(double));
		cfs = (struct coefs * ) malloc (N * sizeof(struct coefs));

		/* values of func on the net */
		for (i = 0; i < N; i ++)
		{
			f[i] = func(2 * M_PI * i / N);
		}

		/* fourie coefs*/
		cos_0 = 0;
		cos_last = 0;
		for (j = 0; j < N; j++)
		{
			cos_0 += f[j] * cos(2 * M_PI * j * 0 / N);
			cos_last += f[j] * cos(2 * M_PI * j * (N/2) / N);
		}
		a_0 = 2.0/N * cos_0;
		a_last = 2.0/N * cos_last;

		for (int k = 1; k < N/2; k ++)
		{
			cfs[k].sin = 0;
			cfs[k].cos = 0;
			for (int j = 0; j < N; j ++)
			{
				cfs[k].sin += 2.0/N * f[j] * sin(2 * M_PI * j * k / N);
				cfs[k].cos += 2.0/N * f[j] * cos(2 * M_PI * j * k / N);
			}
		}
}

net_func::net_func(const int n, double * func_values)
{
	int i, j, k;
		
		double cos_0, cos_last;

		N = n;

		f = (double *) malloc (N * sizeof(double));
		cfs = (struct coefs * ) malloc (N * sizeof(struct coefs));

		/* values of func on the net */
		for (i = 0; i < N; i ++)
		{
			f[i] = func_values[i];
		}

		/* fourie coefs*/
		cos_0 = 0;
		cos_last = 0;
		for (j = 0; j < N; j++)
		{
			cos_0 += f[j] * cos(2 * M_PI * j * 0 / N);
			cos_last += f[j] * cos(2 * M_PI * j * (N/2) / N);
		}
		a_0 = 2.0/N * cos_0;
		a_last = 2.0/N * cos_last;

		for (int k = 1; k < N/2; k ++)
		{
			cfs[k].sin = 0;
			cfs[k].cos = 0;
			for (int j = 0; j < N; j ++)
			{
				cfs[k].sin += 2.0/N * f[j] * sin(2 * M_PI * j * k / N);
				cfs[k].cos += 2.0/N * f[j] * cos(2 * M_PI * j * k / N);
			}
		}
}


net_func::net_func(const net_func & h)
{
	N = h.N;
	a_0 = h.a_0;
	a_last = h.a_last;

	f = (double *) malloc (N * sizeof(double));
	cfs = (struct coefs * ) malloc (N * sizeof(struct coefs));

	for (int i = 0; i < N; i ++)
	{
		f[i] = h.f[i];
	}

	for (int k = 1; k < N/2; k ++)
	{
		cfs[k].cos = h.cfs[k].cos;
		cfs[k].sin = h.cfs[k].sin;
	}
}


net_func::net_func(const net_func  & f, const net_func & g)
{
	double sum = 0;

	N = f.N;

	cfs = (struct coefs * ) malloc (N * sizeof(struct coefs));
	this -> f = (double *) malloc (N * sizeof(double));

	for (int k = 1; k < N/2; k++)
	{
		sum += (f.cfs[k].cos * f.cfs[k].cos) + (f.cfs[k].sin * f.cfs[k].sin) + (g.cfs[k].cos * g.cfs[k].cos) + (g.cfs[k].sin * g.cfs[k].sin);
	}


	a_0 = 1.0/2.0 * (f.a_0 - g.a_0) * (f.a_0 - g.a_0) + 1.0/2.0 * (f.a_last * f.a_last + g.a_last * g.a_last) + sum;
	a_last = (-1) * f.a_last * g.a_last;

	for (int k = 0; k < N/2; k++)
	{
		cfs[k].cos = (-1) * (f.cfs[k].cos * g.cfs[k].cos) - (f.cfs[k].sin * g.cfs[k].sin);
		cfs[k].sin = (f.cfs[k].cos * g.cfs[k].sin) - (f.cfs[k].sin * g.cfs[k].cos);
	}

}



net_func::~net_func()
{
	free(f);
	free(cfs);
}


double net_func::get_sin_coef (const int k)
{
	int j;
	double result = 0;

	for (j = 0; j < N; j++)
	{
		result += f[j] * sin(2 * M_PI * j * k / N);
	}

	return 2.0/N * result;
}

double net_func::get_cos_coef (const int k)
{
	int j;
	double result = 0;

	for (j = 0; j < N; j++)
	{
		result += f[j] * cos(2 * M_PI * j * k / N);
	}

	return 2.0/N * result;
}

double net_func::summate (const int j)
{
	int k;
	double sum = 0;

	// j = 5
	sum += a_0/2;
	sum += a_last/2 * cos(M_PI * j);

	for (k = 1; k < N/2; k ++)
		sum += cfs[k].cos * cos(2 * k * M_PI * j / N) + cfs[k].sin * sin(2 * k * M_PI * j / N);

	return sum;

}

void net_func::print_coefs()
{
	if (cfs != NULL)
	{
		printf("From constructor:\n");
		printf("a_0 = %lf, a_last = %lf\n", a_0, a_last);

		for (int k = 1; k < N/2; k++)
		{
			printf("cos_coef_%d =%lf, sin_coef_%d= %lf\n", k, cfs[k].cos, k, cfs[k].sin);
		}
	}

	else
	{
		printf("a_0 = %lf, a_last = %lf\n", get_cos_coef(0), get_cos_coef(N/2));

		for (int k = 1; k < N/2; k++)
		{
			printf("cos_coef_%d =%lf, sin_coef_%d= %lf\n", k, get_cos_coef(k), k, get_sin_coef(k));
		}
	}
	

	
}


double net_func::summate_rotate(const int j, const int m)
{
	
	int k;
	double sum = 0;

	// j = 5
	sum += a_0/2;
	sum += a_last/2 * cos(M_PI * j) * cos(M_PI * m);

	for (k = 1; k < N/2; k ++)
		sum += cfs[k].cos * (cos(2 * k * M_PI * j / N)*cos(2 * k * M_PI * m / N) - sin(2 * k * M_PI * j / N)*sin(2 * k * M_PI * m / N) ) + cfs[k].sin * (sin(2 * k * M_PI * j / N)*cos(2 * k * M_PI * m / N) + sin(2 * k * M_PI * m / N)*cos(2 * k * M_PI * j / N));

	return sum;
}

void net_func::write_rotation(const int m)
{
	/* file for rotated polygon to be written into*/
	std::ofstream file;
	file.open ("rotated.txt");

	/* polar coords of the rotated border */
	double r = 0, phi = 0;

	/* Descates coords */
	double x = 0, y = 0;
	for (int j = 0; j < N; j++)
	{
		phi = 2 * M_PI * j / N;
		r = summate_rotate(j , m);
		x = r * cos(phi);
		y = r * sin(phi);
		file << x << " " << y << "\n";
	}

	file.close();
}
