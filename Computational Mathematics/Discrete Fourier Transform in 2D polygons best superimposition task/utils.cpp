#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "utils.h"
#include "net_func.h"





double func1(double x)
{
	/*if (x < 0.5)
	{
		return 1;
	}
	else
		return 1;*/
	return x*cos(x);
	
}


double func2(double x)
{
	/*if (x < 0.5)
	{
		return 1;
	}
	else
		return 1;*/
	return x;
	
}



int get_args_N(char ** argv)
{
	int d = 0;
	sscanf(argv[1], "%d", &d);
	return d;
}


int get_args_n(char ** argv, int k)
{
	int d = 0;
	sscanf(argv[k], "%d", &d);
	return d;
}




void get_points(const char * filename, struct point * p, int n)
{
	FILE * f = fopen(filename, "r");

	if (f == NULL)
	{
		printf("Can't open file\n" );
		return;
	}

	for (int i = 0; i < n; i ++)
	{
		fscanf(f, "%lf", &p[i].x);
		fscanf(f, "%lf", &p[i].y);
	}
	fclose(f);
}


void get_lines(double * l, int N)
{
	for (int i = 0; i < N; i++)
	{
		/*l[i] = atan(p[i].y/p[i].x);*/
		l[i] = tan(2 * M_PI * i / N);

	}
}


double get_polar_intersection(struct point p1, struct point p2, double k)
{
	double x = (-1) * (p1.x * (p2.y - p1.y) - p1.y * (p2.x - p1.x)) / ((p2.x - p1.x) * k + (p1.y - p2.y));
	double y = (-1) * (p1.x * (p2.y - p1.y) - p1.y * (p2.x - p1.x)) * k / ((p2.x - p1.x) * k + (p1.y - p2.y));

	return sqrt(x*x + y*y);
}

double get_curr_angle(struct point p)
{
	double phi = atan2(p.y,p.x)*180/M_PI;

	if (phi < 0)
		phi += 360; 
	return phi;
}

void get_func_values (struct point * p, int n, double * l, int N, double * f)
{
	int point_number = 1;
	double phi1 = 0, phi2 = 0, alpha = 0;
	struct point p1, p2;

	p1 = p[0];
	p2 = p[1];
	phi1 = get_curr_angle(p1);
	phi2 = get_curr_angle(p2);
	phi2 += alpha;

	for (int k = 0; k < N; k ++)
	{
		
	
		if ( (2.0 * 180 * k / N + alpha >= phi1 ) && (2.0 * 180 * k / N + alpha < phi2 )  )
			f[k] = get_polar_intersection(p1, p2, l[k]);
		else
		{
			for (int m = point_number; m < n+1; m++)
			{
				if (m == n)
				{
					alpha += 360;
					phi2 += alpha;
					f[k] = get_polar_intersection(p1, p2, l[k]);
					break;
				}
				point_number = (point_number + 1) % n;
				p1 = p2;
				p2 = p[point_number];
				phi1 = get_curr_angle(p1);
				phi2 = get_curr_angle(p2);
				phi2 += alpha;
				if ( (2.0 * 180 * k / N + alpha >= phi1 ) && (2.0 * 180 * k / N + alpha < phi2 )  )
				{
					f[k] = get_polar_intersection(p1, p2, l[k]);
					break;
				}
			}
				
			
		}
	}
}


/*void get_func_values (struct point * p, int n, double * l, int N, double * f)
{
	int point_number = 1;
	double phi1 = 0, phi2 = 0;
	struct point p1, p2;

	p1 = p[0];
	p2 = p[1];

	for (int k = 0; k < N; k ++)
	{
		
		phi1 = atan2(p1.y,p1.x)*180/M_PI;
		phi2 = atan2(p2.y,p2.x)*180/M_PI;
		if (phi1 < 0)
			phi1 += 360;
		if (phi2 < 0)
			phi2 += 360;
		if ((k > N/2) && (phi1 == 0))
			phi1 = 360;
		if ((k > N/2) && (phi2 == 0))
			phi2 = 360;
	
		if ( (2.0 * 180 * k / N >= phi1 ) && (2.0 * 180 * k / N < phi2 )  )
			f[k] = get_polar_intersection(p1, p2, l[k]);
		else
		{
			while(!((2 * 180 * k / N >= phi1 ) && (2 * 180 * k / N < phi2 )))
			{
				point_number = (point_number + 1) % n;
				p1 = p2;
				p2 = p[point_number];
				phi1 = atan2(p1.y,p1.x)*180/M_PI;
				phi2 = atan2(p2.y,p2.x)*180/M_PI;
				if  (phi1 < 0)
					phi1 += 360;
				if (phi2 < 0)
					phi2 += 360;
				if ((k > N/2) && (phi1 == 0))
					phi1 = 360;
				if ((k > N/2) && (phi2 == 0))
					phi2 = 360;
				
			}
				
			f[k] = get_polar_intersection(p1, p2, l[k]);
		}
	}
}*/

void get_values_2(struct point * p, int n, double * l, int N, double * f)
{
	for (int k = 0; k < N/8; k++)
	{
		f[k] = get_polar_intersection(p[3], p[0], l[k]);
	}
	for (int k = N/8; k < 3*N/8; k++)
	{
		f[k] = get_polar_intersection(p[0], p[1], l[k]);
	}
	for (int k = 3*N/8; k < 5*N/8; k++)
	{
		f[k] = get_polar_intersection(p[1], p[2], l[k]);
	}
	for (int k = 5*N/8; k < 7*N/8; k++)
	{
		f[k] = get_polar_intersection(p[2], p[3], l[k]);
	}
	for (int k =7*N/8; k < N; k++)
	{
		f[k] = get_polar_intersection(p[3], p[0], l[k]);
	}
}

void execution_time(const clock_t begin)
{
	clock_t end = clock();
	printf("execution time = %10.3e \n", (double)(end - begin) / CLOCKS_PER_SEC);
}

void centrelize(const int n, struct point * p)
{
	struct point mass_center;
	mass_center.x = 0;
	mass_center.y = 0;

	for (int i = 0; i < n; i++)
	{
		mass_center.x += p[i].x;
		mass_center.y += p[i].y;
	}

	mass_center.x /= n;
	mass_center.y /= n;

	for (int i = 0; i < n; i++)
	{
		p[i].x -= mass_center.x;
		p[i].y -= mass_center.y;
	}
}
