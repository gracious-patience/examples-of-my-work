#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "net_func.h"
#include "utils.h"

int main(int argc, char ** argv)
{
	/* size of a net */
	int N = 0;
	/* rotation parametr */
	int s = 0;
	/* size of the array of vertices of the given figure */
	int n_1 = 0, n_2 = 0;
	struct point * points_1, * points_2;

	double min = 0, value = 0;
	/* a set of lines to make values of a net funcs */
	double * l;
	/* values to initialize funcs from geometry */
	double * f_1, * f_2;
	/* filenames of files with point-sets of the given figures */
	char * filename_1, * filename_2;
	/* for time controlling */
	clock_t begin = 0;

	/* getting args */
	N = get_args_N(argv);
	n_1 = get_args_n(argv, 2);
	n_2 = get_args_n(argv, 4);
	filename_1 = argv[3];
	filename_2 = argv[5];

	/* initializing points, lines, func_values arrays */
	points_1 = (struct point *) malloc (n_1 * sizeof(struct point));
	points_2 = (struct point *) malloc (n_2 * sizeof(struct point));
	l = (double *) malloc (N * sizeof(double));
	f_1 = (double *) malloc (N * sizeof(double));
	f_2 = (double *) malloc (N * sizeof(double));

	/* getting points */
	get_points(filename_1, points_1, n_1);
	get_points(filename_2, points_2, n_2);
	/* mass center to (0,0) */
	centrelize(n_1, points_1);
	centrelize(n_2, points_2);
	/* getting lines */
	get_lines(l, N);
	/* getting func values of borders */
	get_func_values(points_1, n_1, l, N, f_1);
	get_func_values(points_2, n_2, l, N, f_2);
	

	net_func n_f_1(N, f_1);
	net_func n_f_2(N, f_2);

	
	
	net_func rot(n_f_1, n_f_2);

	min = rot.summate(0);
	for (int m = 0; m < N; m++)
	{
		value = rot.summate(m);
		//printf("m = %d, ||.||^2=%lf\n", m, value);
		if (fabs(value) < min)
		{
		//	printf("m = %d, ||.||^2=%lf, min= %lf\n", m, value, min);
			s = m;
			min = value;
		}
			

	}
	

	printf("s=%d, min_norm =%lf\n",s, min );
	execution_time(begin);
	n_f_2.write_rotation(s);
	


	free(points_1);
	free(points_2);
	free(l);
	free(f_1);
	free(f_2);
	return 0;
}
