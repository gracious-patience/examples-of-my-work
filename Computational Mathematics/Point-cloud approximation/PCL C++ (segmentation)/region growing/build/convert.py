import numpy as np
import sys


# func to open .pcd file
def extract_pcd(input_filaname):
    cloud_in_python = []
    with open(input_filaname,'r') as cloud:
        next(cloud)
        next(cloud)
        next(cloud)
        next(cloud)
        next(cloud)
        next(cloud)
        next(cloud)
        next(cloud)
        next(cloud)
        next(cloud)
        next(cloud)
        for line in cloud:
            x, y ,z = [float(x) for x in line.split()]
            cloud_in_python.append([x, y ,z])
    return cloud_in_python

def to_file(filename, points):
    f = open(filename, "w")
    for point in points:
        f.write(str(point[0]) + " " + str(point[1]) + " " + str(point[2]) + "\n")
    f.close

