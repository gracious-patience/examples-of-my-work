#include <iostream>
#include <fstream>
#include <vector>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/parse.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>
//#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/filter_indices.h> // for pcl::removeNaNFromPointCloud
#include <pcl/segmentation/region_growing.h>
#include <pcl/filters/extract_indices.h>

// This function displays the help
void
showHelp(char * program_name)
{
  std::cout << std::endl;
  std::cout << "Usage: " << program_name << " cloud_filename.pcd" << std::endl;
  std::cout << "-h:  Show this help." << std::endl;
}


int
main (int argc, char** argv)
{

  // Show help
  if (pcl::console::find_switch (argc, argv, "-h") || pcl::console::find_switch (argc, argv, "--help")) {
    showHelp (argv[0]);
    return 0;
  }

  // Fetch point cloud filename in arguments | Works with PCD and PLY files
  std::vector<int> filenames;

  filenames = pcl::console::parse_file_extension_argument (argc, argv, ".pcd");

  if (filenames.size () != 1) {
      showHelp (argv[0]);
      return -1;
    }

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

  if (pcl::io::loadPCDFile (argv[filenames[0]], *cloud) < 0)  {
      std::cout << "Error loading point cloud " << argv[filenames[0]] << std::endl << std::endl;
      showHelp (argv[0]);
      return -1;
  }

  pcl::search::Search<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  pcl::PointCloud <pcl::Normal>::Ptr normals (new pcl::PointCloud <pcl::Normal>);
  pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimator;
  normal_estimator.setSearchMethod (tree);
  normal_estimator.setInputCloud (cloud);
  normal_estimator.setKSearch (50);
  normal_estimator.compute (*normals);

  pcl::IndicesPtr indices (new std::vector <int>);
  pcl::ExtractIndices<pcl::PointXYZ> extract;
  pcl::removeNaNFromPointCloud(*cloud, *indices);

  pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
  reg.setMinClusterSize (1000);
  // reg.setMaxClusterSize (2000);
  reg.setSearchMethod (tree);
  reg.setNumberOfNeighbours (30);
  reg.setInputCloud (cloud);
  reg.setIndices (indices);
  reg.setInputNormals (normals);
  reg.setSmoothnessThreshold (0.5 / 180.0 * M_PI);
  reg.setCurvatureThreshold (1.0);

  
  pcl::PointCloud<pcl::PointXYZ>::Ptr cluster_cloud (new pcl::PointCloud<pcl::PointXYZ>), substracted_cloud (new pcl::PointCloud<pcl::PointXYZ>);
  std::vector <pcl::PointIndices> clusters;
  reg.extract (clusters);

  std::cout << "Number of clusters is equal to " << clusters.size () << std::endl;
  // std::cout << "First cluster has " << clusters[0].indices.size () << " points." << std::endl;
  // std::cout << "These are the indices of the points of the initial" <<
  //   std::endl << "cloud that belong to the first cluster:" << std::endl;
  std::size_t counter = 0;
  // while (counter < clusters[0].indices.size ())
  // {
  //   std::cout << clusters[0].indices[counter] << ", ";
  //   counter++;
  //   if (counter % 10 == 0)
  //     std::cout << std::endl;
  // }
  // std::cout << std::endl;


  


  pcl::PCDWriter writer;
  
  std::ofstream myfile;
  myfile.open ("indices_to_subtract.txt");
  for (counter = 0; counter < clusters.size (); ++counter)
  {
    // pcl::PointIndices::Ptr cluster_to_substract(new pcl::PointIndices());
    for (int i = 0; i < clusters[counter].indices.size(); i++) {
      //cluster_to_substract->indices.push_back(clusters[counter].indices[i]);
      myfile << clusters[counter].indices[i] << "\n";
    }
    //std::cout << "New cluster has " << cluster_to_substract->indices.size () << " points." << std::endl;
    pcl::copyPointCloud(*cloud, clusters[counter].indices, *cluster_cloud);
    // extract.setInputCloud(cloud);
    // extract.setIndices(cluster_to_substract);
    // extract.setNegative(true);
    // extract.filter(*cloud);

    std::string output_filename = "cloud_cluster_" + std::to_string(counter) + ".pcd";
    writer.write (output_filename, *cluster_cloud, false);
  }
  // writer.write ("skeleton.pcd", *cloud, false);

  myfile.close();
  return (0);
}
