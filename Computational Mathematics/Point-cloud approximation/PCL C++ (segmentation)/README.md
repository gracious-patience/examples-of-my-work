Главная документация по данному разделу находится тут:

https://pointclouds.org

Среди прочих алгоримтов, реализованных в этой библиотеке, есть различные варианты сегментации большого облака точек на подоблака по некоторому критерию. Желающте установить библиотеку на свой компьютер найдут руководство, перейдя по этой ссылке:

https://pointclouds.org/downloads/

О специальном формате, в который надо преобразовать входные данные, можно прочитать тут:

https://pointclouds.org/documentation/tutorials/pcd_file_format.html

В папках build каждого алгоритма модно найти просто python-скрипт, преобразующий данные .pcd в .txt    .
О том, как собирать проект на UNIX-подобных машиных читайте здесь:

https://pointclouds.org/documentation/tutorials/using_pcl_pcl_config.html
