# USAGE
# python predict.py --input dataset/images/face/image_0131.jpg

from detector import config
from torchvision import transforms
import torch
import argparse
import pickle
import cv2
import imutils

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True, help="path to input image")
args = vars(ap.parse_args())

imagePaths = [args["input"]]

print("[INFO] loading model")
model = torch.load(config.MODEL_PATH).to(config.DEVICE)
model.eval()
le = pickle.loads(open(config.LE_PATHS, "rb").read())

transforms = transforms.Compose([
    transforms.ToPILImage(),
    transforms.ToTensor(),
    transforms.Normalize(config.MEAN, config.STD)
])


for imagePath in imagePaths:
    image = cv2.imread(imagePath)
    orig = image.copy()
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, (224, 224))
    image = image.transpose((2,0,1))

    image = torch.from_numpy(image)
    image = transforms(image).to(config.DEVICE)
    image = image.unsqueeze(0)

    # model work
    (bboxPreds, labelPreds) = model(image)
    (startX, startY, endX, endY) = bboxPreds[0]

    labelPreds = torch.nn.Softmax(dim=-1)(labelPreds)
    i = labelPreds.argmax(dim=-1).cpu()
    label = le.inverse_transform(i)[0]

    # resize the original image such that it fits on our screen, and
	# grab its dimensions
    orig = imutils.resize(orig, width=600)
    (h, w) = orig.shape[:2]
	# scale the predicted bounding box coordinates based on the image
	# dimensions
    startX = int(startX * w)
    startY = int(startY * h)
    endX = int(endX * w)
    endY = int(endY * h)
	# draw the predicted bounding box and class label on the image
    y = startY - 10 if startY - 10 > 10 else startY + 10
    cv2.putText(orig, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX,
		0.65, (0, 255, 0), 2)
    cv2.rectangle(orig, (startX, startY), (endX, endY),
		(0, 255, 0), 2)
	# show the output image 
    cv2.imshow("Output", orig)
    cv2.waitKey(0)