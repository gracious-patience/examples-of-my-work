from matplotlib import image
from detector.bbox_regressor import ObjectDetector
from detector import config
from detector.custom_dataset import VOCDataset
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from torch.utils.data import DataLoader
import torchvision.transforms as transforms
import xml.etree.ElementTree as ET
import argparse
from imutils import paths
import torch
from torchvision.models import resnet50
from tqdm import tqdm
import matplotlib.pyplot as plt
import numpy as np
import pickle
import time
import cv2
import os

ap = argparse.ArgumentParser()
ap.add_argument("-ft", "--finetune", required=True, help="True, if model needs to be finetuned", )
args = vars(ap.parse_args())


print("[INFO] loading dataset")
data = []
labels = []
bboxes = []
imagePaths = []


for xmlPaths in paths.list_files(config.ANNOTS_PATH, validExts=(".xml")):
    mytree = ET.parse(xmlPaths)
    myroot = mytree.getroot()
    filename = myroot.find('filename').text
    for object_in_image in myroot.findall('object'):
        label = object_in_image.find('name').text
        for bounding_box in object_in_image.findall('bndbox'):
            startX = bounding_box.find('xmin').text
            startY = bounding_box.find('ymin').text
            endX = bounding_box.find('xmax').text
            endY = bounding_box.find('ymax').text

            # open image in question
            imagePath = os.path.sep.join([config.IMAGES_PATH,
			filename])
            image = cv2.imread(imagePath)
            (h, w) = image.shape[:2]

            # scale bbox coords
            startX = float(startX) / w
            startY = float(startY) / h
            endX = float(endX) / w
            endY = float(endY) / h

            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            image = cv2.resize(image, (224, 224))

            data.append(image)
            labels.append(label)
            bboxes.append((startX, startY, endX, endY))
            imagePaths.append(imagePath)

# convert the data, class labels, bounding boxes, and image paths to
# NumPy arrays
data = np.array(data, dtype="float32")
labels = np.array(labels)
bboxes = np.array(bboxes, dtype="float32")
imagePaths = np.array(imagePaths)
# perform label encoding on the labels
le = LabelEncoder()
labels = le.fit_transform(labels)

split = train_test_split(data, labels, bboxes, imagePaths,
    test_size=0.2, random_state=42)
(trainImages, testImages) = split[:2]
(trainLabels, testLabels) = split[2:4]
(trainBboxes, testBboxes) = split[4:6]
(trainImagePaths, testImagePaths) = split[6:]

(trainImages, testImages) = torch.tensor(trainImages),\
	torch.tensor(testImages)
(trainLabels, testLabels) = torch.tensor(trainLabels),\
	torch.tensor(testLabels)
(trainBboxes, testBboxes) = torch.tensor(trainBboxes),\
	torch.tensor(testBboxes)

transforms = transforms.Compose([
	transforms.ToPILImage(),
	transforms.ToTensor(),
	transforms.Normalize(mean=config.MEAN, std=config.STD)
])

trainDataSet = VOCDataset((trainImages, trainLabels, trainBboxes), transforms=transforms)
testDataSet = VOCDataset((testImages, testLabels, testBboxes), transforms=transforms)




print("[INFO] total training samples: {}".format(len(trainDataSet)))
print("[INFO] total test samples: {}".format(len(testDataSet)))

trainSteps = len(trainDataSet)//config.BATCH_SIZE
testSteps = len(trainDataSet)//config.BATCH_SIZE

trainDataLoader = DataLoader(trainDataSet, batch_size=config.BATCH_SIZE, 
    shuffle=True,
    # num_workers=os.cpu_count(),
     pin_memory=config.PIN_MEMORY)

testDataLoader = DataLoader(testDataSet, batch_size=config.BATCH_SIZE, 
    # num_workers=os.cpu_count(),
     pin_memory=config.PIN_MEMORY)


# print("[INFO] saving testing image paths...")
# f = open(config.TEST_PATHS, "w")
# f.write("\n".join(testPaths))
# f.close()
data = 0
labels = 0
bboxes = 0
imagePaths = 0

# params of baseline network will be frozen
resnet = resnet50(pretrained=True)
for p in resnet.parameters():
    p.requires_grad = False


if args["finetune"] == "True" or args["finetune"] == "true":
    print("[INFO] model is loading ...")
    objectDetector = torch.load(config.MODEL_PATH).to(config.DEVICE)
else:
    print("[INFO] creating new model ...")
    objectDetector = ObjectDetector(resnet, 20)
    objectDetector.to(config.DEVICE)

classLossFn = torch.nn.CrossEntropyLoss()
bboxLossFn = torch.nn.MSELoss()

optimizer = torch.optim.Adam(objectDetector.parameters(), lr=config.INIT_LR)
print(objectDetector)

H = {"total_train_loss": [], "total_val_loss": [], "train_class_acc": [],
	 "val_class_acc": []}


print("[INFO] training the net")
startTime = time.time()
for e in range(config.NUM_EPOCHS):
    print("[INFO] Epoch number {} started".format(e+1))
    objectDetector.train()
    totalTrainLoss = 0
    totalValLoss = 0
    trainCorrect = 0
    valCorrect = 0

    for (images, labels, bboxes) in trainDataLoader:
        (images, labels, bboxes) = (images.to(config.DEVICE), labels.to(config.DEVICE), bboxes.to(config.DEVICE))

        predictions = objectDetector(images)
        bboxLoss = bboxLossFn(predictions[0], bboxes)
        classLoss = classLossFn(predictions[1], labels)
        totalLoss = (config.BBOX * bboxLoss) + (config.LABELS * classLoss)

        optimizer.zero_grad()
        totalLoss.backward()
        optimizer.step()

        totalTrainLoss += totalLoss.detach()
        trainCorrect += (predictions[1].argmax(1) == labels).type(
			torch.float).sum().item()
        
        # evaluation step
    with torch.no_grad():
        objectDetector.eval()

        for (images, labels, bboxes) in testDataLoader:
            (images, labels, bboxes) = (images.to(config.DEVICE), labels.to(config.DEVICE), bboxes.to(config.DEVICE))

            predictions = objectDetector(images)
            bboxLoss = bboxLossFn(predictions[0], bboxes)
            classLoss = classLossFn(predictions[1], labels)
            totalLoss = (config.BBOX * bboxLoss) + \
                    (config.LABELS * classLoss)
            totalValLoss += totalLoss
            # calculate the number of correct predictions
            valCorrect += (predictions[1].argmax(1) == labels).type(
                  torch.float).sum().item()
    
    # calculate the average training and validation loss
    avgTrainLoss = totalTrainLoss / trainSteps
    avgValLoss = totalValLoss / testSteps
	# calculate the training and validation accuracy
    trainCorrect = trainCorrect / len(trainDataSet)
    valCorrect = valCorrect / len(testDataSet)
	# update our training history
    H["total_train_loss"].append(avgTrainLoss.cpu().detach().numpy())
    H["train_class_acc"].append(trainCorrect)
    H["total_val_loss"].append(avgValLoss.cpu().detach().numpy())
    H["val_class_acc"].append(valCorrect)
	# print the model training and validation information
    print("[INFO] EPOCH: {}/{}".format(e + 1, config.NUM_EPOCHS))
    print("Train loss: {:.6f}, Train accuracy: {:.4f}".format(
		avgTrainLoss, trainCorrect))
    print("Val loss: {:.6f}, Val accuracy: {:.4f}".format(
		avgValLoss, valCorrect))
endTime = time.time()
print("[INFO] total time taken to train the model: {:.2f}s".format(
	endTime - startTime))


# serialize the model to disk
print("[INFO] saving object detector model...")
torch.save(objectDetector, config.MODEL_PATH)
# serialize the label encoder to disk
print("[INFO] saving label encoder...")
f = open(config.LE_PATHS, "wb")
f.write(pickle.dumps(le))
f.close()
# plot the training loss and accuracy
plt.style.use("ggplot")
plt.figure()
plt.plot(H["total_train_loss"], label="total_train_loss")
plt.plot(H["total_val_loss"], label="total_val_loss")
plt.plot(H["train_class_acc"], label="train_class_acc")
plt.plot(H["val_class_acc"], label="val_class_acc")
plt.title("Total Training Loss and Classification Accuracy on Dataset")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="lower left")
# save the training plot
plotPath = os.path.sep.join([config.PLOTS_PATH, "training.png"])
plt.savefig(plotPath)