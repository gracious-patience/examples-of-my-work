import torch

class ObjectDetector(torch.nn.Module):
    def __init__(self, baseModel, numClasses):
        super(ObjectDetector, self).__init__()

        self.baseModel = baseModel
        self.numClasses = numClasses

        self.regressor = torch.nn.Sequential(
            torch.nn.Linear(baseModel.fc.in_features, 128),
            torch.nn.ReLU(),
            torch.nn.Linear(128, 64),
            torch.nn.ReLU(),
            torch.nn.Linear(64, 32),
            torch.nn.ReLU(),
            torch.nn.Linear(32, 4),
            torch.nn.Sigmoid(),
        )

        self.classifier = torch.nn.Sequential(
            torch.nn.Linear(baseModel.fc.in_features, 512),
            torch.nn.ReLU(),
            torch.nn.Dropout(),
            torch.nn.Linear(512, 512),
            torch.nn.ReLU(),
            torch.nn.Dropout(),
            torch.nn.Linear(512, numClasses)
        )

        self.baseModel.fc = torch.nn.Identity()

    def forward(self, x):
        features = self.baseModel(x)
        bboxes = self.regressor(features)
        logits = self.classifier(features)

        return (bboxes, logits)



