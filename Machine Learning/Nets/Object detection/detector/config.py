# a file with hyperparameters of the experiment

from tarfile import LENGTH_LINK
import torch
import os

# path to ouput directory
BASE_PATH = "dataset"
BASE_OUTPUT = "output"
IMAGES_PATH = os.path.sep.join([BASE_PATH, "images"])
ANNOTS_PATH = os.path.sep.join([BASE_PATH, "annotations"])

MODEL_PATH = os.path.sep.join([BASE_OUTPUT, "detector.pth"])
LE_PATHS = os.path.sep.join([BASE_OUTPUT, "le.pickle"])
PLOTS_PATHS = os.path.sep.join([BASE_OUTPUT, "plots"])
TEST_PATHS = os.path.sep.join([BASE_OUTPUT, "test_paths.txt"])

DEVICE = "cuda" if torch.cuda.is_available() else "cpu"
PIN_MEMORY = True if DEVICE == "cuda" else False

# ImageNet MEANS and STDS
MEAN = [0.485, 0.456, 0.406]
STD = [0.229, 0.224, 0.225]

# initialize our initial learning rate, number of epochs to train
# for, and the batch size
INIT_LR = 1e-3
NUM_EPOCHS = 20
BATCH_SIZE = 16
# specify the loss weights
LABELS = 1.0
BBOX = 1.0