from typing import Tuple
import torch
from torch.utils.data import Dataset

class VOCDataset(Dataset):
    def __init__(self, tensors, transforms=None):
        self.tensors = tensors
        self.tranforms = transforms

    def __getitem__(self, index) -> Tuple:
        image = self.tensors[0][index]
        label = self.tensors[1][index]
        bbox = self.tensors[2][index]

        # to PyTorch image format CxHxW
        image = image.permute(2, 0, 1)

        if self.tranforms:
            image = self.tranforms(image)

        return (image, label, bbox)

    def __len__(self):
        return self.tensors[0].size(0)
